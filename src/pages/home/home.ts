import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
import {
  GoogleMaps,
  CameraPosition,
  LatLng,
  GoogleMapsEvent
} from '@ionic-native/google-maps';
import { Geolocation } from "@ionic-native/geolocation"
import { Storage } from '@ionic/storage';
declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  data: string;
  _locationPoints:any;
  _countOfLocations:any;
_loc:LatLng;
 directionsService = new google.maps.DirectionsService;
 directionsDisplay = new google.maps.DirectionsRenderer;
  numberLocation:number=0;
key:string = 'coordinates_of_visited_places';
a:number;

  constructor(
    public navCtrl: NavController,
    private _googleMaps: GoogleMaps,
    private _geoLoc: Geolocation,
    private alertCtrl: AlertController,
    private storage: Storage
    ) { }



async LoadCords(){
  var response = await fetch('https://hahproject.azurewebsites.net/');
  this._locationPoints = await response.json() as Object;
  this._countOfLocations= this._locationPoints.length; 
}
  
  ngAfterViewInit() {
    this.LoadCords()
    this.InitMap();
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.getLocation().then(res => {       
        this._loc = new LatLng(res.coords.latitude, res.coords.longitude);
        this.moveCamera(this._loc); 
       this.startNavigating();
      }).catch(err => {
        console.log(err);
      });
    }
    )
  }
  InitMap() {
    let mapOptions = {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 15,
		}
    let element = this.mapElement.nativeElement;
    this.map = this._googleMaps.create(element, mapOptions)
  }

  getLocation() {
    return this._geoLoc.getCurrentPosition();
  }

  btn_GoTotarget(){

    
    
    this._loc.lat=this._locationPoints[this.numberLocation].X;
    this._loc.lng=this._locationPoints[this.numberLocation].Y;
    console.log( this._loc.lng)
    console.log( this._loc.lat)

    
    
    this.startNavigating();

    
      }

LoadStorage(){
  this.storage.get(this.key).then((val) => {
    console.log(this.key, val);
    this.numberLocation = val;
    console.log(this.numberLocation);
    this.startNavigating();
  });
 
}

presentAlert(){
  let alert = this.alertCtrl.create({
    title: 'Brawo! Jesteś u celu',
    subTitle: 'Wyznaczono kolejny cel podróży',
    buttons: ['Potwierdź']
  });
  alert.present();
 
  this.numberLocation++;
  
  if (this.numberLocation>=this._countOfLocations)
  {
    this.numberLocation=0;
  }
 this.storage.set(this.key, this.numberLocation);

  this.startNavigating();
  
}

  moveCamera(loc: LatLng) {
    let options: CameraPosition<LatLng> = {
      target: loc,
      zoom: 15,
      tilt: 10
    }
    this.map.moveCamera(options);
  }

startNavigating(){
  //this.storage.get(this.key).then((data)=>{this.a=data;});
 
  let mapOptions = {
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
   this.directionsDisplay.setMap(map);
   this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);
  this.directionsService.route({
    origin: this._loc.lat+','+ this._loc.lng,
      destination: this._locationPoints[this.numberLocation].X+','+ this._locationPoints[this.numberLocation].Y,
      travelMode: google.maps.TravelMode['WALKING']
  }, (res, status) => {
    if (res.routes[0].legs[0].distance.value<=200) {
    this.presentAlert()
    }else{
      if(status == google.maps.DirectionsStatus.OK){
          this.directionsDisplay.setDirections(res);
      } else {
          console.warn(status);
      }
    }
  });
}
}



