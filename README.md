# Hare and Hounds


## General info
A mobile game created as a semester project in college. A Hare and Hounds game in which the player must follow the points marked on the Google map.

## Technologies
* Ionic
* Typescript
* C# (server*)

## Status
Project is:  _finished_
*Server is off